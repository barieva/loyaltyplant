package ru.loyaltyplant.moviediscover.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Genre {
    private int id;
}