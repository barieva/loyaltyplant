package ru.loyaltyplant.moviediscover.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
public class MovieInfo {
    @JsonProperty("genre_ids")
    private Set<Integer> genreIds;
    @JsonProperty("vote_average")
    private double voteAverage;
}