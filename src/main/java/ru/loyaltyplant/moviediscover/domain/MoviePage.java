package ru.loyaltyplant.moviediscover.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MoviePage {
    @JsonProperty("total_pages")
    private int totalPages;
    @JsonProperty("results")
    private List<MovieInfo> movieInfos;
}