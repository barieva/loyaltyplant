package ru.loyaltyplant.moviediscover.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GenresHolder {
    List<Genre> genres;
}