package ru.loyaltyplant.moviediscover;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
class MovieDiscoverApplication {
    public static void main(String[] args) {
        SpringApplication.run(MovieDiscoverApplication.class, args);
    }
}