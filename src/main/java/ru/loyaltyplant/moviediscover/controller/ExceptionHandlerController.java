package ru.loyaltyplant.moviediscover.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

import static java.util.Collections.singletonMap;

@RestControllerAdvice
public class ExceptionHandlerController {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<Map<String, String>> sendException(Exception exception) {
        return new ResponseEntity<>(singletonMap("msg", exception.getMessage()), HttpStatus.BAD_REQUEST);
    }
}