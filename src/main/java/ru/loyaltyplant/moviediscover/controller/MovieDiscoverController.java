package ru.loyaltyplant.moviediscover.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.loyaltyplant.moviediscover.service.MovieDiscoverService;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/movie/calculation/")
public class MovieDiscoverController {
    private MovieDiscoverService movieDiscoverService;

    @Autowired
    public void setMovieDiscoverService(MovieDiscoverService movieDiscoverService) {
        this.movieDiscoverService = movieDiscoverService;
    }

    @RequestMapping("start")
    public String start(@RequestParam("id") int genreId) throws ExecutionException, InterruptedException {
        return movieDiscoverService.startCalculating(genreId);
    }

    @GetMapping("progress")
    public String getProgress(@RequestParam("id") int genreId) throws ExecutionException, InterruptedException {
        return movieDiscoverService.getProgress(genreId);
    }

    @GetMapping("stop")
    public boolean stop(@RequestParam("id") int genreId) {
        return movieDiscoverService.stopCalculating(genreId);
    }

    @GetMapping("get")
    public Future<Double> getCalculationResult(@RequestParam("id") int genreId) throws ExecutionException, InterruptedException {
        return movieDiscoverService.getResult(genreId);

    }
}