package ru.loyaltyplant.moviediscover.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.loyaltyplant.moviediscover.domain.Genre;
import ru.loyaltyplant.moviediscover.domain.GenresHolder;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class GenreIdsHolderService {
    private static final Set<Integer> ids = new HashSet<>();
    private RestTemplate restTemplate;
    @Value("${genres-info-url}")
    private String genreInfoUrl;

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Set<Integer> getIds() {
        return ids;
    }

    @PostConstruct
    public void setGenreIds() {
        HttpEntity<GenresHolder> entity = getGenresHolderHttpEntity();
        ResponseEntity<GenresHolder> response = restTemplate.exchange(genreInfoUrl, GET, entity, GenresHolder.class);
        GenresHolder genresHolder = getGenresHolder(response);
        if (genresHolder != null) {
            List<Genre> genres = genresHolder.getGenres();
            for (Genre genre : genres) {
                ids.add(genre.getId());
            }
            log.info("Available genres ids: " + ids);
        }
    }

    private HttpEntity<GenresHolder> getGenresHolderHttpEntity() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_JSON);
        return new HttpEntity<>(headers);
    }

    private GenresHolder getGenresHolder(ResponseEntity<GenresHolder> response) {
        if (OK.equals(response.getStatusCode())) {
            return response.getBody();
        }
        return null;
    }
}