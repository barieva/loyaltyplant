package ru.loyaltyplant.moviediscover.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.loyaltyplant.moviediscover.domain.MovieInfo;
import ru.loyaltyplant.moviediscover.domain.MoviePage;

import javax.annotation.PostConstruct;
import java.text.DecimalFormat;

import static java.lang.Double.parseDouble;
import static java.lang.Thread.currentThread;
import static java.math.RoundingMode.CEILING;

@Service
public class AverageGenreRatingCalculatorService {
    public static final int FIRST_PAGE = 1;
    private MoviePageRetrieverService moviePageRetrieverService;
    private int totalPages;

    @Autowired
    public void setMoviePageRetrieverService(MoviePageRetrieverService moviePageRetrieverService) {
        this.moviePageRetrieverService = moviePageRetrieverService;
    }

    @PostConstruct
    public void setTotalPages() {
        totalPages = moviePageRetrieverService.getMoviePage(FIRST_PAGE).getTotalPages();
    }

    public double calculateAverageGenreRating(int genreId) {
        double ratingSum = 0;
        int moviesCount = 0;
        int pageNumber = FIRST_PAGE;
        while (!currentThread().isInterrupted() && pageNumber < totalPages) {
            MoviePage movieData = moviePageRetrieverService.getMoviePage(pageNumber);
            for (MovieInfo movieInfo : movieData.getMovieInfos()) {
                if (movieInfo.getGenreIds().contains(genreId)) {
                    ratingSum += movieInfo.getVoteAverage();
                    moviesCount++;
                }
            }
            totalPages = movieData.getTotalPages();
            pageNumber++;
        }
        double totalAverageGenreRating = ratingSum / moviesCount;
        return roundToTwoDecimalPlaces(totalAverageGenreRating);
    }

    private double roundToTwoDecimalPlaces(double rating) {
        DecimalFormat format = new DecimalFormat("#.##");
        format.setRoundingMode(CEILING);
        return parseDouble(format.format(rating));
    }
}