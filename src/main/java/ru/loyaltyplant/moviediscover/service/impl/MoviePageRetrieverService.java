package ru.loyaltyplant.moviediscover.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.loyaltyplant.moviediscover.domain.MoviePage;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@Slf4j
public class MoviePageRetrieverService {
    RestTemplate restTemplate;
    @Value("${movies-page-url}")
    private String moviesPageUrl;

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public MoviePage getMoviePage(int page) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_JSON);
        HttpEntity<MoviePage> entity = new HttpEntity<>(headers);
        ResponseEntity<MoviePage> response = restTemplate.exchange(moviesPageUrl + page,
                GET, entity, MoviePage.class);
        MoviePage result = null;
        if (OK.equals(response.getStatusCode())) {
            result = response.getBody();
        }
        log.debug("Get result: {}", result);
        return result;
    }
}