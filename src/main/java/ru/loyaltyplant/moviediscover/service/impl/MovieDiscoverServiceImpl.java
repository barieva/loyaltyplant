package ru.loyaltyplant.moviediscover.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.loyaltyplant.moviediscover.service.MovieDiscoverService;

import java.util.Map;
import java.util.concurrent.*;

import static java.util.concurrent.Executors.newCachedThreadPool;

@Service
@Slf4j
public class MovieDiscoverServiceImpl implements MovieDiscoverService {
    private AverageGenreRatingCalculatorService ratingCalculatorService;
    private ExecutorService executorService = newCachedThreadPool();
    private Map<Integer, Future<Double>> genreIdsWithRatingFuture = new ConcurrentHashMap<>();

    @Autowired
    public void setRatingCalculatorService(AverageGenreRatingCalculatorService ratingCalculatorService) {
        this.ratingCalculatorService = ratingCalculatorService;
    }

    @Override
    public String startCalculating(int genreId) throws ExecutionException {
        if (genreIdsWithRatingFuture.containsKey(genreId)) {
            return getCalculationStatus(genreId);
        } else {
            Future<Double> ratingFuture = executorService.submit(() -> ratingCalculatorService.calculateAverageGenreRating(genreId));
            genreIdsWithRatingFuture.put(genreId, ratingFuture);
            return "Calculation started";
        }
    }

    private String getCalculationStatus(int genreId) throws ExecutionException {
        Future<Double> doubleFuture = genreIdsWithRatingFuture.get(genreId);
        if (doubleFuture.isDone()) {
            try {
                return "Calculation already done. Average rating is: " + doubleFuture.get();
            } catch (InterruptedException | CancellationException e) {
                genreIdsWithRatingFuture.remove(genreId);
                return "Calculation has been stopped. Try again";
            }
        } else {
            return "Calculation in processing. Please wait";
        }
    }

    @Override
    public String getProgress(int genreId) throws ExecutionException {
        if (genreIdsWithRatingFuture.containsKey(genreId)) {
            return getCalculationStatus(genreId);
        } else {
            return "Calculation has not been started";
        }
    }

    /**
     * If no keys found then it calls startCalculating() method and itself again
     */

    @Async
    @Override
    public Future<Double> getResult(int genreId) throws ExecutionException, InterruptedException {
        if (genreIdsWithRatingFuture.containsKey(genreId)) {
            return genreIdsWithRatingFuture.get(genreId);
        } else {
            startCalculating(genreId);
            return getResult(genreId);
        }
    }

    @Override
    public boolean stopCalculating(int genreId) {
        if (genreIdsWithRatingFuture.containsKey(genreId)) {
            return genreIdsWithRatingFuture.get(genreId).cancel(true);
        } else {
            return false;
        }
    }
}