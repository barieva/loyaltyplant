package ru.loyaltyplant.moviediscover.service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public interface MovieDiscoverService {
    String startCalculating(int genreId) throws ExecutionException, InterruptedException;

    String getProgress(int genreId) throws ExecutionException, InterruptedException;

    boolean stopCalculating(int genreId);

    Future<Double> getResult(int genreId) throws ExecutionException, InterruptedException;
}