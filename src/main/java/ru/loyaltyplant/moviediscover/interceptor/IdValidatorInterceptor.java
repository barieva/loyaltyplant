package ru.loyaltyplant.moviediscover.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ru.loyaltyplant.moviediscover.service.impl.GenreIdsHolderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static java.lang.Integer.valueOf;

@Component
public class IdValidatorInterceptor extends HandlerInterceptorAdapter {
    private GenreIdsHolderService genreIdsHolderService;

    @Autowired
    public void setGenreIdsHolderService(GenreIdsHolderService genreIdsHolderService) {
        this.genreIdsHolderService = genreIdsHolderService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Integer id = valueOf(request.getParameter("id"));
        if (isValid(id)) {
            return true;
        } else {
            throw new IllegalArgumentException("Genre id : " + id + " is not valid.");
        }
    }

    private boolean isValid(int genreId) {
        return genreIdsHolderService.getIds().contains(genreId);
    }
}