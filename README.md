# Loyalty Plant

Application can:
Start average rating calculation in the background.  
Get progress of average rating calculation.  
Get result of average rating calculation.  
Stop calculation if it runs.
___
Swagger 

http://localhost:8080/swagger-ui.html
___

Bariev Abdul  
getJavaJob